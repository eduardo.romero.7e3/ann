import tensorflow as tf
import tensorflow_addons as tfa
import tensorflow.keras as keras
import pandas as pd
import matplotlib.pyplot as plt
import numpy as np

def plot_loss(history):
    plt.plot(history.history['loss'], label='loss')
    if history.history.get('val_loss'):
        plt.plot(history.history['val_loss'], label='val_loss', alpha=0.5)
    plt.yscale('log')
    plt.xlabel('Epoch')
    plt.ylabel('Error')
    plt.legend()
    plt.grid(True)
    plt.show()

data = pd.read_csv("happiness_dataset.csv", encoding="utf-16")

x_features = ['X1', 'X2', 'X3', 'X4', 'X5', 'X6']
y_feature = 'D'

train = data.sample(frac=0.8, random_state=2021)
test = data.drop(train.index)

X_train = train.loc[:, x_features]
y_train = train[y_feature]

X_test = test.loc[:, x_features]
y_test = test[y_feature]

normalizer = keras.layers.experimental.preprocessing.Normalization()
normalizer.adapt(np.array(X_train))

model = keras.models.Sequential()
model.add(normalizer)
model.add(tf.keras.layers.Dense(16, activation=tf.nn.relu))
model.add(tf.keras.layers.Dense(16, activation=tf.nn.relu))
model.add(tf.keras.layers.Dense(16, activation=tf.nn.relu))
model.add(tf.keras.layers.Dense(16, activation=tf.nn.relu))
model.add(tf.keras.layers.Dense(2, activation=tf.nn.softmax))

model.compile(optimizer=tf.optimizers.Adam(learning_rate=0.001),
              loss="binary_crossentropy",
              metrics=['accuracy'])

history = model.fit(X_train, y_train, epochs=500, validation_split=0.1)

loss, metric = model.evaluate(X_test, y_test)
print("Loss: ", loss)
print("Accuracy: ", metric)

plot_loss(history)

