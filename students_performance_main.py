import tensorflow as tf
import tensorflow_addons as tfa
import tensorflow.keras as keras
import pandas as pd
import matplotlib.pyplot as plt
import numpy as np

def plot_loss(history):
    plt.plot(history.history['loss'], label='loss')
    if history.history.get('val_loss'):
        plt.plot(history.history['val_loss'], label='val_loss', alpha=0.5)
    plt.yscale('log')
    plt.xlabel('Epoch')
    plt.ylabel('Error')
    plt.legend()
    plt.grid(True)
    plt.show()

data = pd.read_csv('students_performance_dataset.csv', encoding='utf-8')

x_features = ['gender','race/ethnicity','parental level of education','lunch', 'math score','reading score','writing score']
y_feature = 'test preparation course'

data['gender'] = pd.Categorical(data['gender']).codes
data['race/ethnicity'] = pd.Categorical(data['race/ethnicity']).codes
data['parental level of education'] = pd.Categorical(data['parental level of education']).codes
data['lunch'] = pd.Categorical(data['lunch']).codes
data['test preparation course'] = pd.Categorical(data['test preparation course']).codes

train = data.sample(frac=0.8, random_state=2021)
test = data.drop(train.index)

X_train = train.loc[:, x_features]
y_train = train[y_feature]

X_test = test.loc[:, x_features]
y_test = test[y_feature]

normalizer = keras.layers.experimental.preprocessing.Normalization()
normalizer.adapt(np.array(X_train))

model = keras.models.Sequential()
model.add(normalizer)
model.add(tf.keras.layers.Dense(20, activation=tf.nn.relu))
model.add(tf.keras.layers.Dense(1000, activation=tf.nn.relu))
model.add(tf.keras.layers.Dense(20, activation=tf.nn.relu))
model.add(tf.keras.layers.Dense(2, activation=tf.nn.softmax))

model.compile(optimizer=tf.optimizers.Adam(learning_rate=0.00001),
              loss="binary_crossentropy",
              metrics=['accuracy'])

history = model.fit(X_train, y_train, epochs=500, validation_split=0.1)

loss, metric = model.evaluate(X_test, y_test)
print("Loss: ", loss)
print("Accuracy: ", metric)

plot_loss(history)


