import tensorflow as tf
import tensorflow_addons as tfa
import tensorflow.keras as keras
import pandas as pd
import matplotlib.pyplot as plt
import numpy as np

def plot_loss(history):
    plt.plot(history.history['loss'], label='loss')
    if history.history.get('val_loss'):
        plt.plot(history.history['val_loss'], label='val_loss', alpha=0.5)
    plt.yscale('log')
    plt.xlabel('Epoch')
    plt.ylabel('Error')
    plt.legend()
    plt.grid(True)
    plt.show()

data = pd.read_csv('videogames_dataset.csv', encoding='utf-8')

x_features = ['console','alcohol_reference','animated_blood','blood','blood_and_gore','cartoon_violence','crude_humor',
              'drug_reference','fantasy_violence','intense_violence','language','lyrics','mature_humor','mild_blood',
              'mild_cartoon_violence','mild_fantasy_violence','mild_language','mild_lyrics','mild_suggestive_themes',
              'mild_violence','no_descriptors','nudity','partial_nudity','sexual_content','sexual_themes',
              'simulated_gambling','strong_janguage','strong_sexual_content','suggestive_themes','use_of_alcohol',
              'use_of_drugs_and_alcohol','violence']
y_feature = 'esrb_rating'

data['esrb_rating'] = pd.Categorical(data['esrb_rating']).codes

train = data.sample(frac=0.8, random_state=2021)
test = data.drop(train.index)

X_train = train.loc[:, x_features]
y_train = train[y_feature]

X_test = test.loc[:, x_features]
y_test = test[y_feature]

model = keras.models.Sequential()
model.add( keras.layers.Dense(32, activation=tf.nn.relu))
model.add(keras.layers.Dropout(0.4))
model.add(tf.keras.layers.Dense(4, activation=tf.nn.softmax))

model.compile(optimizer=tf.optimizers.Adam(learning_rate=0.0001),
              loss="sparse_categorical_crossentropy",
              metrics=['accuracy'])

history = model.fit(X_train, y_train, epochs=400, validation_split=0.2)

loss, metric = model.evaluate(X_test, y_test)
print("Loss: ", loss)
print("Accuracy: ", metric)

plot_loss(history)


