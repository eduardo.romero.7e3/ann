import tensorflow as tf
import tensorflow_addons as tfa
import tensorflow.keras as keras
import pandas as pd
import matplotlib.pyplot as plt
import numpy as np

def plot_loss(history):
    plt.plot(history.history['loss'], label='loss')
    if history.history.get('val_loss'):
        plt.plot(history.history['val_loss'], label='val_loss', alpha=0.5)
    plt.yscale('log')
    plt.xlabel('Epoch')
    plt.ylabel('Error')
    plt.legend()
    plt.grid(True)
    plt.show()

data = pd.read_csv("camera_dataset.csv", encoding="utf-8")

x_features = ["Max resolution", "Low resolution", "Effective pixels",
              "Zoom wide (W)", "Zoom tele (T)", "Normal focus range",
              "Macro focus range", "Storage included", "Weight (inc. batteries)", "Dimensions"]
y_feature = "Price"

data = data.dropna()
X = data.loc[:, x_features]
y = data[y_feature]

X_train = X.sample(frac=0.8, random_state=2021)
X_test = X.drop(X_train.index)

y_train = y[X_train.index]
y_test = y[X_test.index]

normalizer = keras.layers.experimental.preprocessing.Normalization()
normalizer.adapt(np.array(X_train))

model = keras.models.Sequential()
model.add(normalizer)

model.add(tf.keras.layers.Dense(80, activation=tf.nn.relu))
model.add(tf.keras.layers.Dense(60, activation=tf.nn.relu))
model.add(tf.keras.layers.Dense(1))

model.compile(optimizer=tf.optimizers.Adam(learning_rate=0.0001),
              loss="mean_squared_error",
              metrics=tfa.metrics.RSquare(y_shape=(1,)))

history = model.fit(X_train, y_train, epochs=300, validation_split=0.2)

loss, metric = model.evaluate(X_test, y_test)
print("Loss: ", loss)
print("R-Square: ", metric)
plot_loss(history)





